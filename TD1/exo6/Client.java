/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import static java.lang.Math.floor;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lounis
 */
public class Client {

	/**
	 * @param args the command line arguments
	 */

	public static void afficher_allu(int n) {
		int i;
		System.out.print("\n");
		for (i = 0; i < n; i++)
			System.out.print("  o");
		System.out.print("\n");
		for (i = 0; i < n; i++)
			System.out.print("  |");
		System.out.print("\n");
		for (i = 0; i < n; i++)
			System.out.print("  |");
		System.out.print("\n");

	}
	public static int jeu_ordi(int nb_allum, int prise_max) {
		int prise = 0;
		int s = 0;
		float t = 0;
		s = prise_max + 1;
		t = ((float) (nb_allum - s)) / (prise_max + 1);
		while (t != floor(t)) {
			s--;
			t = ((float) (nb_allum - s)) / (prise_max + 1);
		}
		prise = s - 1;
		if (prise == 0)
			prise = 1;
		return (prise);
	}

	public static void main(String[] args) {
		int nb_max_d = 0; /* nbre d'allumettes maxi au départ */
		int nb_allu_max = 0; /* nbre d'allumettes maxi que l'on peut tirer au maxi */
		int qui = 0; /* qui joue? 0=Nous --- 1=PC */
		int prise = 0; /* nbre d'allumettes prises par le joueur */
		int nb_allu_rest = 0; /* nbre d'allumettes restantes */
		boolean isEnd = false;
		String receivedMessage = "";
		try {
			Socket client = new Socket("localhost", 8080);
			DataOutputStream out = new DataOutputStream(client.getOutputStream());
			DataInputStream in = new DataInputStream(client.getInputStream());
			Scanner sc = new Scanner(System.in);
			do {
				System.out.println("Nombre d'allumettes disposées entre les deux joueurs (entre 10 et 60) :");
				nb_max_d = sc.nextInt();
			} while ((nb_max_d < 10) || (nb_max_d > 60));
			out.writeInt(nb_max_d);
			out.flush();
			do {
				System.out.println("\nNombre maximal d'allumettes que l'on peut retirer : ");
				nb_allu_max = sc.nextInt();
				if (nb_allu_max >= nb_max_d)
					System.out.println("Erreur !");
			} while ((nb_allu_max >= nb_max_d) || (nb_allu_max == 0));
			out.writeInt(nb_allu_max);
			out.flush();
			do {
				System.out.println("\nQuel joueur commence? 0--> Joueur ; 1--> Ordinateur : ");
				qui = sc.nextInt();
				if ((qui != 0) && (qui != 1))
					System.out.println("\nErreur");
			} while ((qui != 0) && (qui != 1));
			out.writeInt(qui);
			out.flush();
			// Start game
			do {
				// 1 - Receive allu_rest
				nb_allu_rest = in.readInt();
				System.out.println("\nNombre d'allumettes restantes :" + nb_allu_rest);
				afficher_allu(nb_allu_rest);
				
				// 2 - Send prise depend on qui
				if (qui == 0) {
					do {
						System.out.println("\nCombien d'allumettes souhaitez-vous piocher ? ");
						prise = sc.nextInt();
						if ((prise > nb_allu_rest) || (prise > nb_allu_max)) {
							System.out.println("Erreur !\n");
						}
					} while ((prise > nb_allu_rest) || (prise > nb_allu_max));
				} else {
					prise = jeu_ordi(nb_allu_rest, nb_allu_max);
					System.out.println("\nPrise de l'ordi :" + prise);
				}
				out.writeInt(prise);
				out.flush();
				
				// 3 - Update qui
				qui = in.readInt();
				
				// 4 - WTF
				isEnd = in.readBoolean();
			} while (!isEnd);
			System.out.println(in.readUTF());
			try {
				Thread.sleep(20);
			} catch (InterruptedException ex) {
				Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
			}
			in.close();
			out.close();
			client.close();

		} catch (IOException ex) {
			Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

}
