/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
/**
 *
 * @author lounis
 */
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import static java.lang.Math.floor;
import java.util.logging.Logger;

public class Server {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		int nb_allu_rest = 0; /* nbre d'allumettes restantes */
		int nb_allu_max = 0; /* nbre d'allumettes maxi que l'on peut tirer au maxi */
		int qui = 0; /* qui joue? 0=Nous --- 1=PC */
		int prise = 0; /* nbre d'allumettes prises par le joueur */
		String s = "\nCombien d'allumettes souhaitez-vous piocher ? ";
		try {
			ServerSocket conn = new ServerSocket(8080);
			Socket comm = conn.accept();
			DataOutputStream out = new DataOutputStream(comm.getOutputStream());
			DataInputStream in = new DataInputStream(comm.getInputStream());
			nb_allu_rest = in.readInt();
			nb_allu_max = in.readInt();
			qui = in.readInt();
			System.out.println("Start with: " + qui);
			do {
				// 1 - Send allu_rest
				out.writeInt(nb_allu_rest);
				out.flush();
				
				// 2 - Receive prise
				prise = in.readInt();
				nb_allu_rest = nb_allu_rest - prise;
				System.out.println("Left:" + nb_allu_rest);
				
				// 3 - Send qui
				qui = (qui + 1) % 2;
				out.writeInt(qui);
				out.flush();
				
				// 4 - WTF
				out.writeBoolean(nb_allu_rest == 0);
				out.flush();
			} while (nb_allu_rest > 0);
			if (qui == 0) /* Cest à nous de jouer */
				out.writeUTF("\nVous avez gagné!\n");
			else
				out.writeUTF("\nVous avez perdu!\n");
			in.close();
			out.close();
			comm.close();
		}catch(
	IOException ex)
	{
		Logger.getLogger(ServerSocket.class.getName()).log(Level.SEVERE, null, ex);
	}

}

}
