# Compte-rendu SR03 TD3
## Yuxuan WANG & Jian SUN
---
### Partie B
#### Injection SQL

**\$req="select nom,prenom,login,id_user,numero_compte,profil_user,solde_compte from users where login='\$login' and mot_de_passe='\$pwd'";**
C'est facile de se connecter au site avec username 'or 1=1# (un espace) de ce type d'injection.  

**\$req="update users set solde_compte=solde_compte+\$mt where numero_compte='\$dest'";**  
**\$req="update users set solde_compte=solde_compte-\$mt where numero_compte='\$src'";**  
Si on remplit le champs N° compte destinataire avec **' or 1=1 #**, la demande est valide.  

**\$req="insert into messages(id_user_to,id_user_from,sujet_msg,corps_msg) values(\$to,\$from,'\$subject','\$body')";**  
Si on remplit le champs Sujet avec **test',(select version())) #**, la demande est valide et on obtient un mail avec sujet **test** et message **5.7.26(la version de mysql)**.  

#### Cross-site scripting (XSS)
##### a
On peut envoyer un message à un utilisateur avec le contenu:  `<script>alert(“hey!you are attacked”)</script>`. Lorsqu'il lit le message, il va recevoir une attaque.
##### b
Un pirate stocke un code actif sur le serveur. Si un utilisateur demande une ressource au niveau du serveur, il reçoit le code actif et l'information de l'utilisateur est volé par le pirate. Par exemple, le pirate stocke une redirection frauduleux à un utilisateur. Si l'utilisateur est redirigé vers le lien, l'information est volé.  
##### c
On est peut-être protégé de XSS mais pas le site?

#### Violation de contrôle d’accès
##### Le système d’authentification par login et mot de passe
Chaque fois il faudrait saisir le login et le mot de passe pour pouvoir voir le contenu d'un page, ce qui prend du temps et a un risque de vol d'information.
##### Le système de contrôle d’accès
Il faudrait avoir un très bonnne gestion de droit au niveau du site. Sinon, des utilisateurs ne peuvent pas avoir le bon page.
Si les demandes ne sont pas vérifiées, les attaquants seront en mesure de forger des demandes afin d'accéder à une fonctionnalité non autorisée.  
##### Le système de référence directe
Si contrôle d'accès n'est pas bien protégé, les attaquants peuvent manipuler ces références pour accéder à des données non autorisées. Dans ce cas-là, il faudrait aussi avoir un très bonnne gestion de droit.

#### Violation de gestion de session
En copiant l'url http://localhost:8888/sr03p20/vw_moncompte.php du page mon compte, j'ai pu accéder à la session de l'utilisateur depuis un autre navigateur. 
Je me connecte sur mon compte en utilisant le navigateur Chrome et puis je copie l'url   http://localhost:8888/sr03p20/vw_moncompte.php dans le navigateur Safari, j'ai pu y accéder.  
J'ai effectué un transfert de 2000 sur Chrome, ce qui me fait un solde de 1000.  
En suite, j'ai fait un transfert de 100 sur Safari, ce qui me donne un solde de 2900.  
Je me reconnecte sur mon compte dans 2 navigateur, le solde est 2900 à la fin.  
Le résultat dépend à la dernière opétation qui a été effectuée.
#### Falsification de requête (CSRF)
La fonctionnalité de transfert ou celle d'envoie d'un mail.  
Il peut y avoir un autre site qui réalise une telle attque. Après avoir se connecté sur le page moncompte, si l'utilisateur ouvre le site au frauduleux, le site au frauduleux peut effectuer une action POST et qui fait un transfert de montant ou un envoie de mail.  
` <form method="POST" name="transfer"　action="http://localhost:8888/sr03p20/myController.php">`  
`　　　　　　　　<input type="hidden" name="destination" value="numero_compte">`  
`　　　　　　　　<input type="hidden" name="montant" value="1000">`  
`</form>`  
#### Vulnérabilité d'un composant
PHP et mysql peuvent possède des des vulnérabilités. Au niveau du PHP, on peut utiliser un framework comme Sympony pour sécuriser l'application. Au niveau de mysql, on peut utiliser mySQLi pour évider SQL injection.
#### Chiffrement des données sensibles
Le mot de passe peut être chiffré au cours du stockage dans la base de données.
#### Accès aux répertoires par http
`http://localhost:8888/sr03p20/config/config.ini`
#### Scripts de redirection

