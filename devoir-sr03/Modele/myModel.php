<?php


function getMySqliConnection() {
  $db_connection_array = parse_ini_file("../config/config.ini");
  return new mysqli($db_connection_array['DB_HOST'], $db_connection_array['DB_USER'], $db_connection_array['DB_PASSWD'], $db_connection_array['DB_NAME']);
}


function findUserByLoginPwd($login, $pwd) {
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
      $utilisateur = false;
  } else {
    // Ici on utilise Prepared Statements pour éviter injection sql.
      $req = "select nom,prenom,login,id_user,numero_compte,profil_user,solde_compte,mot_de_passe from users where (login= ?)";
      // Préparer notre requête.
      $stmt = $mysqli->prepare($req);
      // Passer les paramètres. "ss" indique que les params login et pwd sont tous de type string.
      $stmt->bind_param("s", $login);
      // Exécution de la requête.
      if (!$stmt->execute()) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
          $utilisateur = false;
      } else {
        $result=$stmt->get_result();
          if ($result->num_rows === 0) {
            $utilisateur = false;
          } else {
            $utilisateur = $result->fetch_assoc();
            if(!password_verify($pwd,$utilisateur['mot_de_passe'])){
              $utilisateur=false;
            }
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $utilisateur;
}


function findAllUsers() {
  $mysqli = getMySqliConnection();

  $listeUsers = array();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
      $req="select nom,prenom,login,id_user from users";
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      } else {
          while ($unUser = $result->fetch_assoc()) {
            $listeUsers[$unUser['id_user']] = $unUser;
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $listeUsers;
}

function findAllEmploye() {
  $mysqli = getMySqliConnection();

  $listeUsers = array();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
      $req="select nom,prenom,login,id_user from users where profil_user='EMPLOYE'";
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      } else {
          while ($unUser = $result->fetch_assoc()) {
            $listeUsers[$unUser['id_user']] = $unUser;
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $listeUsers;
}

function findAllClient() {
  $mysqli = getMySqliConnection();

  $listeUsers = array();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
      $req="select nom,prenom,login,id_user,numero_compte, solde_compte from users where profil_user='CLIENT'";
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      } else {
          while ($unUser = $result->fetch_assoc()) {
            $listeUsers[$unUser['id_user']] = $unUser;
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $listeUsers;
}

function transfert($dest, $src, $mt) {
  $mysqli = getMySqliConnection();
  $flag = false;
  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
      $utilisateur = false;
  } else {
    // Par défault, la transaction est automatiquement commit. On le met à false.
    $mysqli->autocommit(false);
    // On utilise Prepared Statements pour éviter injection sql. 
    $req1="update users set solde_compte=solde_compte+? where numero_compte=?";
    $stmt1=$mysqli->prepare($req1);
    $stmt1->bind_param("is",$mt,$dest);

    $req2="update users set solde_compte=solde_compte-? where numero_compte=?";
    $stmt2=$mysqli->prepare($req2);
    $stmt2->bind_param("is",$mt,$src);
    // Exécution des requêtes.
    if(!$stmt1->execute()||!$stmt2->execute()){
      echo 'Erreur requête BDD ['.$req1.$req2.'] (' . $mysqli->errno . ') '. $mysqli->error;
      $mysqli->rollback();
    }else{
      $mysqli->commit();
      $flag=true;
    }
    $mysqli->close();
  }

  return $flag;
}


function findMessagesInbox($userid) {
  $mysqli = getMySqliConnection();

  $listeMessages = array();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
      $req="select id_msg,sujet_msg,corps_msg,u.nom,u.prenom from messages m, users u where m.id_user_from=u.id_user and id_user_to=".$userid;
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      } else {
          while ($unMessage = $result->fetch_assoc()) {
            $listeMessages[$unMessage['id_msg']] = $unMessage;
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $listeMessages;
}


function addMessage($to,$from,$subject,$body) {
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
      $req="insert into messages(id_user_to,id_user_from,sujet_msg,corps_msg) values($to,$from,'$subject','$body')";
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      }
      $mysqli->close();
  }

}

function login_record($flag) {
  $mysqli = getMySqliConnection();
  if ($mysqli->connect_error) {
    echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
    //ip2long() convertir l'adresse ip en  chiffre
    $ip=$_SERVER['REMOTE_ADDR'];
    // Ici on utilise Prepared Statements pour éviter injection sql.
    $req = "insert into login_info(id, ipaddr, logintime, pass_status) values(NULL,'$ip',CURRENT_TIMESTAMP,$flag)";
    // Exécution de la requête.      
    if (!$result = $mysqli->query($req)) {
      echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;      
    }
    $mysqli->close();
  }
}

function check_fail_time() {
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
      //ip2long() convertir l'adresse ip en  chiffre
      $ip=$_SERVER['REMOTE_ADDR'];
      // Ici on utilise Prepared Statements pour éviter injection sql.
      $req = "SELECT logintime,pass_status FROM login_info WHERE( ipaddr='$ip')ORDER BY id DESC";
      // Exécution de la requête.
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      }else{
        $listlogin_info=array();
        if($result->num_rows<3){
          $flag=1;
        }else{
          $interval_fail_check_min=0;
          $interval_fail_check_sec=20;
          //timezone
          date_default_timezone_set('Europe/Paris');
          $time_now=date("Y-m-d H:i:s");
          $flag_status=0;
          $flag_timestamp=0;
          for($i=0;$i<3;$i++){
            $row=$result->fetch_assoc();
            $time_login=$row["logintime"];
            $time1=strtotime($time_now);
            $time2=strtotime($time_login);            
            if($time1-$time2  > $interval_fail_check_min*60+$interval_fail_check_sec){
              $flag_timestamp+=1;
            }
            $flag_status+=$row["pass_status"];
          }
          if($flag_status==0 && $flag_timestamp==0){
            $flag=0;
          }else{
            $flag=1;
          }
        }
        $result->free();
      }
      $mysqli->close();
  }
  return $flag;
}


?>
