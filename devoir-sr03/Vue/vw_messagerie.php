<?php
  if(  !isset($_SERVER['HTTP_REFERER'])  || strpos($_SERVER['HTTP_REFERER'], '.php') == false){
    die('<html><h1><center> Je le savais !!!<br> Ce n\'est pas gentil.<br>C\'est <font color="red">INTERDIT</font>.</center> </h1><meta http-equiv="refresh" content="5; ../index.php"> </html>');
  
  }
  session_start();
  if (!isset($_SESSION['CREATED'])) {
    $_SESSION['CREATED'] = time();
    } else if (time() - $_SESSION['CREATED'] > 20) {
    // session started more than 20 s ago 
    session_regenerate_id(true); // change session ID for the current session an invalidate old session ID
    $_SESSION['CREATED'] = time(); // update creation time
    unset($_SESSION["connected_user"]);
  }
  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {
    // utilisateur non connecté
    header('Location: ../index.php');      
  }
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Messagerie</title>
  <link rel="stylesheet" type="text/css" media="all"  href="../css/mystyle.css" />
</head>
<body>
    <header>
        <form method="POST" action="../Controleur/myController.php">
            <input type="hidden" name="action" value="disconnect">
            <input type="hidden" name="loginPage" value="../Vue/vw_login.php?disconnect">
            <button class="btn-logout form-btn">Déconnexion</button>
        </form>

        <form method="POST" action="../Controleur/myController.php">
            <input type="hidden" name="action" value="accueil">
            <button class="btn-accueil form-btn">Accueil</button>
        </form>

        <h2><?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?> - Messagerie</h2>
    </header>

    <section>

    <article>
        <form method="POST" action="../Controleur/myController.php">
          <input type="hidden" name="action" value="sendmsg">
          <div class="fieldset">
              <div class="fieldset_label">
                  <span>Envoyer un message</span>
              </div>
              <div class="field">
                  <label>Destinataire : </label>
                  <select name="to">
                    <?php
                    foreach ($_SESSION['listeUsers'] as $id => $user) {
                      echo '<option value="'.$id.'">'.$user['nom'].' '.$user['prenom'].'</option>';
                    }
                    ?>
                  </select>
              </div>
              <div class="field">
                  <label>Sujet : </label><input type="text" size="20" name="sujet">
              </div>
              <div class="field">
                  <label>Message : </label><textarea name="corps" cols="25" rows="3"></textarea>
              </div>
              <button class="form-btn">Envoyer</button>
              <?php
              if (isset($_REQUEST["msg_ok"])) {
                echo '<p>Message envoyé avec succès.</p>';
              }
              ?>
          </div>
        </form>
        </article>


        <article>
          <div class="liste">
            <table>
              <tr><th>Expéditeur</th><th>Sujet</th><th>Message reçu</th></tr>
              <?php
              foreach ($_SESSION['messagesRecus'] as $cle => $message) {
                echo '<tr>';
                echo '<td>'.$message['nom'].' '.$message['prenom'].'</td>';
                echo '<td>'.$message['sujet_msg'].'</td>';
                echo '<td>'.$message['corps_msg'].'</td>';
                echo '</tr>';
              }
               ?>
            </table>
          </div>
        </article>
    </section>
</body>
</html>
