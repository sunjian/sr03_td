<?php
  if(  !isset($_SERVER['HTTP_REFERER'])  || strpos($_SERVER['HTTP_REFERER'], '.php') == false){
    die('<html><h1><center> Je le savais !!!<br> Ce n\'est pas gentil.<br>C\'est <font color="red">INTERDIT</font>.</center> </h1><meta http-equiv="refresh" content="5; ../index.php"> </html>');
  
  }
  session_start();
  if (!isset($_SESSION['CREATED'])) {
    $_SESSION['CREATED'] = time();
    } else if (time() - $_SESSION['CREATED'] > 20) {
    // session started more than 20 s ago 
    session_regenerate_id(true); // change session ID for the current session an invalidate old session ID
    $_SESSION['CREATED'] = time(); // update creation time
    unset($_SESSION["connected_user"]);
  }
  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {
    // utilisateur non connecté
    header('Location: ../index.php');      
  }
  if(isset($_REQUEST["virementUser"])){
    $virementUser=$_REQUEST["virementUser"];
  }else{
    $virementUser="";
  }
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Accueil</title>
  <link rel="stylesheet" type="text/css" media="all"  href="../css/mystyle.css" />
</head>
<body>
    <header>
        <form method="POST" action="../Controleur/myController.php">
            <input type="hidden" name="action" value="disconnect">
            <input type="hidden" name="loginPage" value="../Vue/vw_login.php?disconnect">
            <button class="btn-logout form-btn">Déconnexion</button>
        </form>

        <form method="POST" action="../Controleur/myController.php">
            <input type="hidden" name="action" value="accueil">
            <button class="btn-accueil form-btn">Accueil</button>
        </form>
        
        <h2><?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?> - Virement</h2>
    </header>
    
    <section>
              
        <article>
          <div class="fieldset">
              <div class="fieldset_label">
                  <span>Votre compte</span>
              </div>
              <div class="field">
                  <label>N° compte : </label><span><?php echo $_SESSION["connected_user"]["numero_compte"];?></span>
              </div>
              <div class="field">
                  <label>Solde : </label><span><?php echo $_SESSION["connected_user"]["solde_compte"];?> &euro;</span>
              </div>
          </div>
        </article>
        
        <article>
        <form method="POST" action="../Controleur/myController.php">
          <input type="hidden" name="action" value="transfert">
          <div class="fieldset">
              <div class="fieldset_label">
                  <span>Transférer de l'argent</span>
              </div>
              <div class="field">
                  <label>N° compte destinataire : </label><input type="text" value="<?php echo $virementUser;?>" size="20" name="destination">
              </div>
              <div class="field">
                  <label>Montant à transférer : </label><input type="text" size="10" name="montant">
              </div>
              <button class="form-btn">Transférer</button>
              <?php
              if (isset($_REQUEST["trf_ok"])) {
                echo '<p>Virement effectué avec succès.</p>';
              }
              if (isset($_REQUEST["bad_mt"])) {
                echo '<p>Le montant saisi est incorrect : '.$_REQUEST["bad_mt"].'</p>';
              }
              if (isset($_REQUEST["notenough"])) {
                echo '<p>Vous n\'avez pas assez d\'argent.</p>';
              }
              if (isset($_REQUEST["self"])) {
                echo '<p>Vous ne pouvez pas faire un virement à vous-même.</p>';
              }
              ?>
          </div>
        </form>
        </article>
        

        
    </section>

</body>
</html>
