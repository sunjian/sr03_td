<?php
  session_start();
  if (!isset($_SESSION['CREATED'])) {
    $_SESSION['CREATED'] = time();
    } else if (time() - $_SESSION['CREATED'] > 20) {
    // session started more than 20 s ago 
    session_regenerate_id(true); // change session ID for the current session an invalidate old session ID
    $_SESSION['CREATED'] = time(); // update creation time
    unset($_SESSION["connected_user"]);
  }
  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {
    // utilisateur non connect�
    header('Location: vw_login.php');      
  }
  if (!isset($_SESSION['HTTP_USER_AGENT'])  ||  md5($_SERVER['HTTP_USER_AGENT']) != $_SESSION['HTTP_USER_AGENT']){ 
    die('<html><h1><center> Je le savais !!!<br> Ce n\'est pas gentil.<br> C\'est <font color="red">INTERDIT</font>.<br> </center> </h1> </html>');
  }
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Accueil de <?php echo $_SESSION["connected_user"]["prenom"]?> <?php echo $_SESSION["connected_user"]["nom"];?></title>
  <link rel="stylesheet" type="text/css" media="all"  href="../css/mystyle.css" />
</head>
<body>
    <header>
        <form method="POST" action="../Controleur/myController.php">
            <input type="hidden" name="action" value="disconnect">
            <input type="hidden" name="loginPage" value="../Vue/vw_login.php?disconnect">
            <button class="btn-logout form-btn">Déconnexion</button>
        </form>
        
        <h2><?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?> - Mon compte</h2>
    </header>
    
    <section>
      
        <article>
          <div class="fieldset">
              <div class="fieldset_label">
                  <span>Vos informations personnelles</span>
              </div>
              <div class="field">
                  <label>Login : </label><span><?php echo $_SESSION["connected_user"]["login"];?></span>
              </div>
              <div class="field">
                  <label>Profil : </label><span><?php echo $_SESSION["connected_user"]["profil_user"];?></span>
              </div>
          </div>
        </article>
        
        <article>
          <div class="fieldset">
              <div class="fieldset_label">
                  <span>Votre compte</span>
              </div>
              <div class="field">
                  <label>N° compte : </label><span><?php echo $_SESSION["connected_user"]["numero_compte"];?></span>
              </div>
              <div class="field">
                  <label>Solde : </label><span><?php echo $_SESSION["connected_user"]["solde_compte"];?> &euro;</span>
              </div>
          </div>
        </article>
        
        <article>
          <div class="fieldset">
              <form method="POST" action="../Controleur/myController.php">
                <input type="hidden" name="action" value="virer">
                <input type="hidden" name="virementPage" value="../Vue/vw_virement.php">
                <button class="form-btn">Transférer de l'argent</button>
              </form>
          </div>
        </article>

        <article>
          <div class="fieldset">
              <form method="POST" action="../Controleur/myController.php">
                <input type="hidden" name="action" value="messagerie">
                <button class="form-btn">Messagerie</button>
              </form>
          </div>
        </article>

        <?php if ($_SESSION["connected_user"]["profil_user"]=="EMPLOYE") echo '
        <article>
        <div class="fieldset">
        <form method="POST" action="../Controleur/myController.php">
        <input type="hidden" name="action" value="fiche">
        <button class="form-btn">La fiche de client</button>
        </form>
        </div>
        </article>
        ';?>
        
    </section>

</body>
</html>
