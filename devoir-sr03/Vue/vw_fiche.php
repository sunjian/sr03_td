<?php
  if(  !isset($_SERVER['HTTP_REFERER'])  || strpos($_SERVER['HTTP_REFERER'], '.php') == false){
    die('<html><h1><center> Je le savais !!!<br> Ce n\'est pas gentil.<br>C\'est <font color="red">INTERDIT</font>.</center> </h1><meta http-equiv="refresh" content="5; ../index.php"> </html>');
  
  }
  session_start();
  if (!isset($_SESSION['CREATED'])) {
    $_SESSION['CREATED'] = time();
    } else if (time() - $_SESSION['CREATED'] > 20) {
    // session started more than 20 s ago 
    session_regenerate_id(true); // change session ID for the current session an invalidate old session ID
    $_SESSION['CREATED'] = time(); // update creation time
    unset($_SESSION["connected_user"]);
  }
  if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {
    // utilisateur non connect�
    header('Location: ../index.php');      
  }
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Messagerie</title>
  <link rel="stylesheet" type="text/css" media="all"  href="../css/mystyle.css" />
</head>
<body>
    <header>
         <form method="POST" action="../Controleur/myController.php">
            <input type="hidden" name="action" value="disconnect">
            <input type="hidden" name="loginPage" value="../Vue/vw_login.php?disconnect">
            <button class="btn-logout form-btn">Déconnexion</button>
        </form>

        <form method="POST" action="../Controleur/myController.php">
            <input type="hidden" name="action" value="accueil">
            <button class="btn-accueil form-btn">Accueil</button>
        </form>

        <h2><?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?> - Fiche des Clients</h2>
    </header>

    <section>
        <article>
        
          <div class="liste">
            <table>
              <tr><th>Nom</th><th>Prenom</th><th>Login</th><th>NumDeCompte</th><th>Solde</th><th>LienDeVirement</th></tr>
              <?php
              foreach ($_SESSION['listeClients'] as $cle => $client) {
                echo '<tr>';
                echo '<td>'.$client['nom'].'</td>';
                echo '<td>'.$client['prenom'].'</td>';
                echo '<td>'.$client['login'].'</td>';
                echo '<td>'.$client['numero_compte'].'</td>';
                echo '<td>'.$client['solde_compte'].'</td>';
                echo '<td>'.'<center><form method="POST" action="../Controleur/myController.php">
                <input type="hidden" name="action" value="virer">
                <input type="hidden" name="virementPage" value="../Vue/vw_virement.php?virementUser='.$client['numero_compte'].'">
                <button class="form-btn">effectuer un virement</button>
            </form></center>'.'</td>';
                echo '</tr>';
              }
               ?>
            </table>
          </div>
    
        </article>
    </section>
</body>
</html>
