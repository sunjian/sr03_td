<?php
  if(  !isset($_SERVER['HTTP_REFERER'])  || strpos($_SERVER['HTTP_REFERER'], '.php') == false){
    die('<html><h1><center> Je le savais !!!<br> Ce n\'est pas gentil.<br>C\'est <font color="red">INTERDIT</font>.</center> </h1><meta http-equiv="refresh" content="5; ../index.php"> </html>');
  
  }
    require_once('../Modele/myModel.php');
    session_start();
    if (!isset($_SESSION['HTTP_USER_AGENT'])  ||  md5($_SERVER['HTTP_USER_AGENT']) != $_SESSION['HTTP_USER_AGENT']){ 
        die('<html><h1><center> Je le savais !!!<br> Ce n\'est pas gentil.<br> C\'est <font color="red">INTERDIT</font>.<br> </center> </h1> </html>');
      }
      if (!isset($_SESSION['CREATED'])) {
        $_SESSION['CREATED'] = time();
        } else if (time() - $_SESSION['CREATED'] > 20) {
        // session started more than 20 s ago 
        session_regenerate_id(true); // change session ID for the current session an invalidate old session ID
        $_SESSION['CREATED'] = time(); // update creation time
        unset($_SESSION["connected_user"]);
      }
    if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {
        // utilisateur non connecté
        header('Location: ../index.php');      
    }
        
    // URL de redirection par défaut (si pas d'action ou action non reconnue)
    $url_redirect = "index.php";
    //Id de l'utilisateur actuel
    
    if (isset($_REQUEST['action'])) {
        
        if ($_REQUEST['action'] == 'authenticate') {
            /* ======== AUTHENT ======== */
            //return 0 if so many fails
            if(check_fail_time()){

                if (!isset($_REQUEST['login']) || !isset($_REQUEST['mdp']) || $_REQUEST['login'] == "" || $_REQUEST['mdp'] == "") {
                    // manque login ou mot de passe
                    $url_redirect = "../Vue/vw_login.php?nullvalue";
                    // record each try
                    login_record(0);
                    
                } else {
                    $utilisateur = findUserByLoginPwd($_REQUEST['login'], $_REQUEST['mdp']);
                    
                    if ($utilisateur == false) {
                        // echec authentification
                        $url_redirect = "../Vue/vw_login.php?badvalue";
                        login_record(0);
                        
                    } else {
                        // authentification réussie
                        $_SESSION["connected_user"] = $utilisateur;
                        if($_SESSION["connected_user"]["profil_user"]=="CLIENT"){
                            $_SESSION["listeUsers"] = findAllEmploye();
                        }else{
                            $_SESSION["listeUsers"] = findAllUsers() ;
                            $_SESSION["listeClients"]=findAllClient();
                        }
                        $url_redirect = "../Vue/vw_accueil.php";
                        login_record(1);
                    }
                }

            }else{
                $url_redirect = "../Vue/vw_login.php?lock";
            }
            
        } else if ($_REQUEST['action'] == 'disconnect') {
            /* ======== DISCONNECT ======== */
            unset($_SESSION["connected_user"]);
            $url_redirect = $_REQUEST['loginPage'] ;
            
        } else if ($_REQUEST['action'] == 'accueil') {
            /* ======== ACCUEIL ======== */
            $url_redirect = "../Vue/vw_accueil.php" ;   
            
        } else if ($_REQUEST['action'] == 'transfert') {
            /* ======== TRANSFERT ======== */
            if (is_numeric ($_REQUEST['montant'])) {
                if($_SESSION["connected_user"]["solde_compte"]<$_REQUEST['montant']){
                    $url_redirect = "../Vue/vw_virement.php?notenough";

                }else if($_REQUEST['destination']==$_SESSION["connected_user"]["numero_compte"]){
                    $url_redirect = "../Vue/vw_virement.php?self";
                }else{
                    transfert($_REQUEST['destination'],$_SESSION["connected_user"]["numero_compte"], $_REQUEST['montant']);
                    $_SESSION["connected_user"]["solde_compte"] = $_SESSION["connected_user"]["solde_compte"] -  $_REQUEST['montant'];
                    $url_redirect = "../Vue/vw_virement.php?trf_ok";
                }                
            } else {
                $str1 = trim($_REQUEST['montant']); // Filtrer espace                 
                $str1 = strip_tags($str1); // Filtrer balise HTML                 
                $str1 = htmlspecialchars($str1); // Convertit les caractères spéciaux en entités HTML                  
                $str1 = addslashes($str1); // Eviter injection SQL
                $url_redirect = "../Vue/vw_virement.php?bad_mt=".$str1;
            }
            
        } else if (($_REQUEST['action'] == 'sendmsg')||($_REQUEST['action'] == 'messagerie')){
            /* ======== MESSAGE ======== */
            $userid=$_SESSION["connected_user"]["id_user"];
            if($_REQUEST['action'] == 'sendmsg'){
                $str1 = trim($_REQUEST['sujet']); // Filtrer espace              
                $str1 = strip_tags($str1); // Filtrer balise HTML     
                $str1 = htmlspecialchars($str1); // Convertit les caractères spéciaux en entités HTML         
                $str1 = addslashes($str1); // Eviter injection SQL
                $str2 = trim($_REQUEST['corps']); // Filtrer espace         
                $str2 = strip_tags($str2); // Filtrer balise HTML
                $str2 = htmlspecialchars($str2); // Convertit les caractères spéciaux en entités HTML          
                $str2 = addslashes($str2); // Eviter injection SQL
                addMessage($_REQUEST['to'],$_SESSION["connected_user"]["id_user"],$str1,$str2);
                $url_redirect = "../Vue/vw_messagerie.php?msg_ok";
            }else{
                $url_redirect = "../Vue/vw_messagerie.php"; 
            }
            $_SESSION['messagesRecus'] = findMessagesInbox($userid); 
            
        } else if($_REQUEST['action'] == 'virer'){
            /*virement page*/
            $url_redirect = $_REQUEST['virementPage'];
            
        } else if($_REQUEST['action'] == 'fiche'){
            /*fiche page*/
            $url_redirect = "../Vue/vw_fiche.php";
        } 
        
    }  
    
    header("Location: $url_redirect");
    
    ?>