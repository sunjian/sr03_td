

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Validation
 */
@WebServlet("/Validation")
public class Validation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Validation() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
        String firstName = request.getParameter("User first name");
        String lastName = request.getParameter("User family name");
        String mail = request.getParameter("User email");
        String gender = request.getParameter("gender");
        String password = request.getParameter("User password");
        
        // System.out.println(UserManager.getUsersTable().containsValue(new User(lastName, firstName)));
        boolean isValid = true;
		if ((firstName == null || lastName == null || mail == null || password == null)
				|| (firstName == null || lastName == null || mail == null || password == null)) {
			isValid = false;
			RequestDispatcher rd = request.getRequestDispatcher("newUser.html");
			rd.forward(request, response);
		}
		if (UserManager.getUsersTable().containsValue(new User(lastName, firstName))) {
			isValid = false;
			try (PrintWriter out = response.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet Validation</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>User already exists, you still want to be added?  </h1>");
                out.println("<form method='POST' action='Validation'>");
                out.println("Yes <input type=\"radio\" name=\"userAdded\" value=\"yep\" /> ");
                out.println("No <input type=\"radio\" name=\"userAdded\" value=\"nop\" />");
                out.println("<input type='hidden' name='User first name' value='" + firstName + "'/>");
                out.println("<input type='hidden' name='User family name' value='" + lastName + "'/>");
                out.println("<input type='hidden' name='User email' value='" + mail + "'/>");
                out.println("<input type='hidden' name='gender' value='" + gender + "'/>");
                out.println("<input type='hidden' name='User password' value='" + password + "' />");
                out.println("<br>");
                out.println("<input type ='submit' value='Send' name='validator' />");
                out.println("</form>");
                out.println("</body>");
                out.println("</html>");
            }
		}
		if (request.getParameter("validator") != null) {
            if ("yep".equals(request.getParameter("userAdded"))) {
                isValid = true;
            } 
            else {
                isValid = false;
                RequestDispatcher rd = request.getRequestDispatcher("newUser.html");
                rd.forward(request, response);
            }
		}
		
		if (isValid) {
			RequestDispatcher rd = request.getRequestDispatcher("UserManager");
            rd.forward(request, response);
        }
		
		
	}
	


}
