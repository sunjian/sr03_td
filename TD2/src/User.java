
public class User {
	private String lastName; 
    private String firstName;
    private String login;
    private String gender;
    private String pwd;
    private int id;
    private String role;

	public User(String lastName, String firstName) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
	}
	public User(String lastName, String firstName, String login, String gender, String pwd, int id, String role) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.login = login;
		this.gender = gender;
		this.pwd = pwd;
		this.id = id;
		this.role = role;
	}
	public User(String lastName, String firstName, String login, String gender, String pwd, int id) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.login = login;
		this.gender = gender;
		this.pwd = pwd;
		this.id = id;
	}

	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "User [lastName=" + lastName + ", firstName=" + firstName + ", login=" + login + ", gender=" + gender
				+ ", pwd=" + pwd + ", id=" + id + ", role=" + role + "]";
	}
	

}
