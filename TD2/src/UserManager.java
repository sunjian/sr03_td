

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserManager
 */
@WebServlet("/UserManager")
public class UserManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Hashtable<Integer, User> usersTable = new Hashtable<Integer, User>();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserManager() {
        super();
    }

    

	public static Hashtable<Integer, User> getUsersTable() {
		return usersTable;
	}



	public static void setUsersTable(Hashtable<Integer, User> usersTable) {
		UserManager.usersTable = usersTable;
	}



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title> Users </title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> List of users: </h1>");
            out.println("<ol>");
            Set<Integer> keys = usersTable.keySet();
            for(Integer key: keys){
            	out.println("<li>");
                out.println(usersTable.get(key).toString()); 
                out.println("</li>");
            }
            out.println("</ol>");
            out.println("</body>");
            out.println("</html>");
        }
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
        String firstName = request.getParameter("User first name");
        String lastName = request.getParameter("User family name");
        String mail = request.getParameter("User email");
        String gender = request.getParameter("gender");
        String password = request.getParameter("User password");  
        String role = request.getParameter("admin");
        usersTable.put(usersTable.size(), new User(lastName, firstName, mail, gender, password, usersTable.size(), role));
		doGet(request, response);
		
        
	}

}
